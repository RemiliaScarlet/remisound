require "./src/remisound"
require "../remiaudio/src/remiaudio"

if ARGV.size < 1 || ARGV.size > 2
  abort "Usage: #{Path[PROGRAM_NAME].basename} <WAV file> [driver]

If [driver] is not specified, then libao is used.  Drivers are:
  * libao
  * portaudio
  * pulseaudio"
end

drv = if ARGV.size == 1
        "libao" # Default
      else
        ARGV[1].downcase
      end

# Plays the requested WAV file using the device *dev*.
def playWav(dev : RemiSound::AudioDevice) : Nil
  puts "Opening #{ARGV[0]}"

  # Open a WAV reader.
  RemiAudio::Formats::WavFile.open(ARGV[0]) do |strm|
    # Check that it's 44.1Khz stereo (any bit depth)
    unless strm.sampleRate == 44100 && strm.channels == 2
      abort "Only 44100 Hz stereo WAV files supported (any bit depth)"
    end

    # The buffer we'll write into.
    buf = Slice(Float32).new(2048, 0.0f32)
    idx = 0
    curSize = 0

    # Read samples from the WAV into the buffer, then send to the device.
    while idx < strm.numSamples
      curSize = Math.min(2048, strm.numSamples - idx)
      curSize.times do |i|
        buf[i] = strm.read.to_f32!
      end

      # Write the buffer
      dev.writeBuffer(buf[...curSize])
    end
  end
end

# Determine which device we want, then play the WAV with it
case drv
when "libao"
  RemiSound.withDevice(RemiSound::AoDevice, 44100, 16, 2) do |dev|
    dev.bufferSize = 1024
    dev.start
    puts "Libao Driver, using #{dev.name} (driver ID #{dev.driverID})"
    playWav(dev)
  end
when "portaudio"
  RemiSound.withDevice(RemiSound::PortDevice, 44100, 16, 2) do |dev|
    dev.bufferSize = 1024
    dev.start
    puts "PortAudio Driver, using buffer size of #{dev.bufferSize}"
    playWav(dev)
  end
when "pulseaudio"
  RemiSound.withDevice(RemiSound::PulseDevice, 44100, 32, 2) do |dev|
    dev.bufferSize = 1024
    dev.programName = "RemiAudio WAV Player"
    dev.streamName = "RemiAudio WAV"
    dev.start
    puts "PulseAudio Driver"
    playWav(dev)
  end
else
  abort "Bad driver name"
end

#### RemiSound
#### Copyright (C) 2023 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./remisound/audiodevice"

# RemiSound is a small library that abstracts away various audio
# output libraries. # It currently supports PortAudio, PulseAudio, and
# libao.
module RemiSound
  VERSION = "0.1.0"

  # Creates a new device instance, then yields it to the block.  This ensures
  # that `#stop` is called once the block exits.
  #
  # `T` must be a subclass of `RemiSound::AudioDevice`.
  def self.withDevice(typ : T.class, sampleRate : Int, bitDepth : Int, channels : Int, &) : Nil forall T
    {% begin %}
      {% unless T < ::RemiSound::AudioDevice %}
        {% raise "Must use a subclass of RemiSound::AudioDevice" %}
      {% end %}
    {% end %}
    dev = T.new(sampleRate, bitDepth, channels)
    yield dev
  ensure
    dev.try &.stop
  end
end

#### RemiSound
#### Copyright (C) 2023 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

module RemiSound
  class AudioDeviceError < Exception
  end

  # The `AudioDevice` class is a virtual representation of an audio output
  # device.  Subclasses exist for various "driver" backends, such as libao,
  # PortAudio, etc.
  #
  # Whenever possible, the implementing class attempts to work entirely using
  # 32-bit floating point audio, and the `#writeBuffer` method only accepts
  # 32-bit floating point.  However, some backends (e.g. libao) do not support
  # floating point audio natively, so the corresponding subclass will convert
  # audio automatically on the fly.  See the documentation for these subclasses
  # for more information.
  abstract class AudioDevice
    # Returns the sample rate that this device will request from the underlying
    # backend when started.  The audio data sent to `#writeBuffer` should match
    # this sample rate.
    getter sampleRate : UInt32 = 44100u32

    # Returns the bit depth that this device will request from the underlying
    # backend when started.
    getter bitDepth : UInt8 = 16u8

    # Returns the number of output channels that this device will request from
    # the underlying backend when started.  The audio data sent to
    # `#writeBuffer` should match this.
    getter channels : UInt8 = 2u8

    # The size of the audio buffers that will get written.  This is per-channel,
    # so if this is 2048 and there are two channels, then you need an array of
    # 4096 elements for a buffer.
    #
    # This MUST NOT change after the driver has been started, and you MUST
    # ALWAYS pass the correct buffer size to `#writeBuffer`.
    getter bufferSize : UInt32 = 2048u32

    # The expected size of the audio buffers that you send to `#writeBuffer`.
    # This value changes depending on what you set for `#bufferSize`.
    getter expectedBufferSize : UInt32 = 4096u32

    # Returns `true` if the device has been started, or `false` otherwise.
    getter? started : Bool = false

    @outputSize : UInt32 = 4096u32 * sizeof(Float32)

    private def initialize
      @sampleRate = 44100u32
      @bitDepth = 16u8
      @channels = 2u8
      @outputSize = 0
      @expectedBufferSize = 0
    end

    def bufferSize=(value : UInt32) : Nil
      if @started
        raise AudioDeviceError.new("Cannot change the buffer size after the device has been started")
      end
      @bufferSize = value
      @expectedBufferSize = @bufferSize * @channels
      @outputSize = @expectedBufferSize * sizeof(Float32)
    end

    # Creates a new device instance with the requested parameters.
    abstract def initialize(newSampleRate : Int, newBitDepth : Int, newChannels : Int)

    # Opens the audio stream.  This must be called before `#writeBuffer` is
    # called.
    abstract def start : Nil

    # Closes the audio stream and frees resources.  This must be called when you
    # are finished using the instance.
    abstract def stop : Nil

    # Plays back the audio in *buf* by sending it to the underlying backend.
    #
    # You MUST ALWAYS pass the correct buffer size to `#writeBuffer`, as defined
    # by the value of `#bufferSize` multiplied by the number of `#channels`.
    abstract def writeBuffer(buf : Array(Float32)|Slice(Float32)) : Nil
  end
end

require "./drivers/*"

#### RemiSound
#### Copyright (C) 2023 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

{% begin %}
  {% unless flag?(:remisound_no_libao) %}
    @[Link(ldflags: "`pkg-config ao --libs`")]
    lib Libao
      ENODRIVER   = 1
      ENOTFILE    = 2
      ENOTLIVE    = 3
      EBADOPTION  = 4
      EOPENDEVICE = 5
      EOPENFILE   = 6
      EFILEEXISTS = 7
      EBADFORMAT  = 8

      enum AoType
        Live = 1
        File = 2
      end

      enum AoFormat
        Little = 1
        Big = 2
        Native = 4
      end

      alias PDevice = Pointer(Void)
      alias CStr = Pointer(UInt8)

      struct AoInfo
        typ : LibC::Int
        name : CStr
        shortName : CStr
        author : CStr
        comment : CStr
        preferredByteFormat : LibC::Int
        priority : LibC::Int
        options : Pointer(CStr)
        optionCount : LibC::Int
      end

      struct AoOption
        key : CStr
        value : CStr
        nxt : Pointer(AoOption)
      end

      struct AoSampleFormat
        bits : LibC::Int
        rate : LibC::Int
        channels : LibC::Int
        byteFormat : LibC::Int
        matrix : CStr
      end

      fun init = ao_initialize() : Void
      fun shutdown = ao_shutdown() : Void
      fun isBigEndian = ao_is_big_endian : LibC::Int
      fun appendOption = ao_append_option(options : Pointer(Pointer(AoOption)), key : CStr, value : CStr) : LibC::Int
      fun appendGlobalOption = ao_append_global_option(key : CStr, value : CStr) : LibC::Int
      fun freeOptions = ao_free_options(options : Pointer(AoOption)) : Void
      fun openLive = ao_open_live(driverID : LibC::Int, fmt : Pointer(AoSampleFormat),
                                  options : Pointer(AoOption)) : PDevice
      fun play = ao_play(device : PDevice, outputsamples : Pointer(UInt8), numBytes : UInt32) : LibC::Int
      fun close = ao_close(device : PDevice) : LibC::Int
      fun driverID = ao_driver_id(shortName : CStr) : LibC::Int
      fun defaultDriverID = ao_default_driver_id() : LibC::Int
      fun driverInfo = ao_driver_info(driverID : LibC::Int) : Pointer(AoInfo)
      fun driverInfoList = ao_driver_info_list(driverCount : Pointer(LibC::Int)) : Pointer(Pointer(AoInfo))
      fun fileExtension = ao_file_extension(driverID : LibC::Int) : CStr
    end
  {% end %}
{% end %}

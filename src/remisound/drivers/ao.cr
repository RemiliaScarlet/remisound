#### RemiSound
#### Copyright (C) 2023 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "../audiodevice"
require "./ao-lib"

module RemiSound
  {% begin %}
    {% unless flag?(:remisound_no_libao) %}
      # The `AoDevice` class is used to playback audio using libao.
      #
      # As libao does not support output to Float32 directly, this class will
      # convert the audio to the requested bit rate on-the-fly.  It does this
      # without any dithering.
      class AoDevice < AudioDevice
        enum DriverType
          Live = 1
          File = 2
        end

        enum Format
          Little = 1
          Big = 2
          Native = 4
        end

        ##
        ## Fields that can be set
        ##

        getter driverID : Int32 = 0
        getter matrix : String = "L,R"

        ##
        ## Informational Fields
        ##

        getter type : DriverType = DriverType::Live
        getter name : String = ""
        getter shortName : String = ""
        getter author : String = ""
        getter comment : String = ""
        getter preferredByteFormat : Format = Format::Native
        getter priority : Int32 = 0
        getter availableOptions : Array(String) = [] of String

        ##
        ## Internal Fields
        ##

        @devicePtr : Libao::PDevice = Libao::PDevice.null
        @foreignBuf : Array(UInt8)|Array(Int16) = Array(UInt8).new(0, 0u8)
        @sampleMult : Int32 = 0

        def initialize(newSampleRate : Int, newBitDepth : Int, newChannels : Int)
          @sampleRate = newSampleRate.to_u32.as(UInt32)
          @bitDepth = newBitDepth.to_u8.as(UInt8)
          @channels = newChannels.to_u8.as(UInt8)
          @expectedBufferSize = @bufferSize * @channels
          @outputSize = @expectedBufferSize * sizeof(Float32)
        end

        def driverID=(id : Int32) : Nil
          if @started
            raise AudioDeviceError.new("Cannot change the driver ID after the device has been started")
          end
          @driverID = value
        end

        def matrix=(value : String) : Nil
          if @started
            raise AudioDeviceError.new("Cannot change the matrix after the device has been started")
          end
          @matrix = value
        end

        def start : Nil
          raise AudioDeviceError.new("Attempted to start a device twice") if @started
          Libao.init

          id = driverID || Libao.defaultDriverID
          info : Pointer(Libao::AoInfo) = Libao.driverInfo(id)
          @driverID = id
          @type = DriverType.from_value?(info[0].typ) ||
                  raise AudioDeviceError.new("Could not determine the driver type for driver ID #{id}")
          @name = String.new(info[0].name)
          @shortName = String.new(info[0].shortName)
          @author = String.new(info[0].author)
          @comment = String.new(info[0].comment)
          @preferredByteFormat = Format.from_value?(info[0].preferredByteFormat) ||
                                 raise AudioDeviceError.new("Could not determine preferred byte format for " \
                                                            "driver ID #{id}")
          @priority = info[0].priority

          info[0].optionCount.times do |idx|
            name = String.new(info[0].options[idx])
            @availableOptions << name
          end

          case @bitDepth
          when 8
            @sampleMult = Int8::MAX.to_i32!
          when 16
            @sampleMult = Int16::MAX.to_i32!
          when 24
            @sampleMult = 8388607 # (1- (expt 2 23))
          else raise AudioDeviceError.new("AoDevice: Bit depth is unsupported for this library: #{@bitDepth}")
          end

          spec : Libao::AoSampleFormat = Libao::AoSampleFormat.new
          spec.bits = @bitDepth
          spec.rate = @sampleRate.to_i32
          spec.channels = @channels
          spec.byteFormat = @preferredByteFormat.value
          spec.matrix = @matrix.to_unsafe

          @devicePtr = Libao.openLive(@driverID, pointerof(spec), Pointer(Libao::AoOption).null)
          @started = true
        end

        def stop : Nil
          unless @devicePtr.null?
            Libao.close(@devicePtr)
            @devicePtr = Libao::PDevice.null
          end
          Libao.shutdown if @started
          @started = false
        end

        @[AlwaysInline]
        private def fillBuf(buf : Array(Float32)|Slice(Float32)) : UInt32
          len : Int32 = buf.size
          foreignLen : Int32 = len
          neededLen : Int32 = len

          case @bitDepth
          when 8
            if @foreignBuf.size != neededLen
              @foreignBuf = Array(UInt8).new(neededLen, 0u8)
            end

            fbuf = @foreignBuf.as(Array(UInt8))
            len.times do |i|
              fbuf[i] = (buf[i] * @sampleMult).to_u8!
            end

          when 16
            foreignLen = len * 2
            if @foreignBuf.size != neededLen
              @foreignBuf = Array(Int16).new(neededLen, 0i16)
            end

            fbuf16 = @foreignBuf.as(Array(Int16))
            len.times do |i|
              fbuf16[i] = (buf[i] * @sampleMult).to_i16!
            end
          when 24
            foreignLen = len * 3
            neededLen = foreignLen
            if @foreignBuf.size != neededLen
              @foreignBuf = Array(UInt8).new(neededLen, 0u8)
            end

            di : Int32 = 0
            smp : Float32 = 0.0f32
            fbuf = @foreignBuf.as(Array(UInt8))
            len.times do |i|
              smp = buf.unsafe_fetch(i)
              fbuf[di    ] =  ((smp * @sampleMult).to_i32! & 0x0000FF).to_u8!
              fbuf[di + 1] = (((smp * @sampleMult).to_i32! & 0x00FF00) >> 8).to_u8!
              fbuf[di + 2] = (((smp * @sampleMult).to_i32! & 0xFF0000) >> 16).to_u8!
              di += 3
            end
          else raise AudioDeviceError.new("Bad bit depth")
          end

          foreignLen.to_u32!
        end

        def writeBuffer(buf : Array(Float32)|Slice(Float32)) : Nil
          {% unless flag?(:remisound_wd40) %}
            raise AudioDeviceError.new("Device not started") unless @started
            unless buf.size == @expectedBufferSize
              raise AudioDeviceError.new("Buffer was the incorrect size: #{buf.size} != #{@expectedBufferSize}")
            end
          {% end %}
          len : UInt32 = fillBuf(buf)
          Libao.play(@devicePtr, @foreignBuf.to_unsafe.unsafe_as(Pointer(UInt8)), len)
        end
      end
    {% end %}
  {% end %}
end

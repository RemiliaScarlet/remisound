#### RemiSound
#### Copyright (C) 2023 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "../audiodevice"
require "./pulse-simple-lib"

module RemiSound
  {% begin %}
    {% unless flag?(:remisound_no_pulseaudio) %}
      # The `PulseDevice` class is used to playback audio using PulseAudio.
      #
      # PulseAudio supports Float32 audio natively, so the `#bitDepth` field is
      # ignored by this class and is always set to `32`.
      class PulseDevice < AudioDevice
        getter programName : String = "RemiSound Program"
        getter streamName : String = "RemiSound Stream"
        @streamPtr : Pulse::PaSimple = Libao::PDevice.null

        def initialize(newSampleRate : Int, newBitDepth : Int, newChannels : Int)
          @sampleRate = newSampleRate.to_u32.as(UInt32)
          @bitDepth = 32u8 # newBitDepth is ignored by PulseDevice
          @channels = newChannels.to_u8.as(UInt8)
          @expectedBufferSize = @bufferSize * @channels
          @outputSize = @expectedBufferSize * sizeof(Float32)
        end

        def programName=(value : String) : Nil
          if @started
            raise AudioDeviceError.new("Cannot change the program name after the device has been started")
          end
          @programName = value
        end

        def streamName=(value : String) : Nil
          if @started
            raise AudioDeviceError.new("Cannot change the stream name after the device has been started")
          end
          @streamName = value
        end

        def start : Nil
          raise AudioDeviceError.new("Attempted to start a device twice") if @started
          spec = Pulse::PaSampleSpec.new
          spec.format = Pulse::PaSampleFormat::PA_SAMPLE_FLOAT32LE
          spec.channels = @channels
          spec.rate = @sampleRate

          @streamPtr = Pulse.pa_simple_new(nil,
                                           programName,
                                           Pulse::PaStreamDirection::PA_STREAM_PLAYBACK,
                                           nil, # default device
                                           streamName,
                                           pointerof(spec),
                                           nil, # Default channel map
                                           nil, # Defaiult buffering attributes
                                           out err)

          if @streamPtr.null?
            errStr : String = String.new(Pulse.pa_strerror(err))
            raise AudioDeviceError.new("Could not initialize PulseAudio: #{errStr}")
          end
          @started = true
        end

        def stop : Nil
          Pulse.pa_simple_free(@streamPtr) unless @streamPtr.null?
          @started = false
        end

        def writeBuffer(buf : Array(Float32)|Slice(Float32)) : Nil
          {% unless flag?(:remisound_wd40) %}
            raise AudioDeviceError.new("Device not started") unless @started
            unless buf.size == @expectedBufferSize
              raise AudioDeviceError.new("Buffer was the incorrect size: #{buf.size} != #{@expectedBufferSize}")
            end
          {% end %}
          result = Pulse.pa_simple_write(@streamPtr, buf.to_unsafe, @outputSize, out writeError)
          unless result == 0
            errStr = String.new(Pulse.pa_strerror(writeError))
            raise AudioDeviceError.new("Could not write data to PulseAudio: #{errStr}")
          end
        end
      end
    {% end %}
  {% end %}
end

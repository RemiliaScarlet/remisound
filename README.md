# RemiSound

RemiSound is a small library that abstracts away various audio output libraries.
It currently supports PortAudio, PulseAudio, and libao.

## Version Control

RemiSound sources are managed using [Fossil](https://www.fossil-scm.org/), a
distributed version control system. The [Fossil
repository](https://nanako.mooo.com/fossil/remisound) contains the urtext,
tickets, and wiki.

If you are reading this on GitLab or some other Git repository or service, then
you are looking at a mirror. The names of check-ins and other artifacts in a Git
mirror are different from the official names for those objects. The official
names for check-ins are found in a footer on the check-in comment for authorized
mirrors. The official check-in name can also be seen in the manifest.uuid file
in the root of the tree. Always use the official name, not the Git-name, when
communicating about a RemiSound check-in.

## How do I get set up?

Add this to your `shard.yml`, then run `shards update`:

```yaml
dependencies:
  remisound:
    fossil: https://nanako.mooo.com/fossil/remisound
```

## Usage

```crystal

# Replace PortDevice with PulseDevice or AoDevice as desired. 
# Note that AoDevice will require a bit depth of 8, 16, or 24.
RemiSound.withDevice(RemiSound::PortDevice, 44100, 32, 2) do |dev|
  dev.bufferSize = 1024
  dev.start
  # ... Start sending data using dev.writeBuffer ...
end

# Or, alternatively, do the setup/teardown yourself:
dev = RemiSound::PortDevice.new(44100, 32, 2)
dev.bufferSize = 1024
dev.start

# ... Start sending data using dev.writeBuffer ...

# Stop the device.  This must be done to properly
# close the device and release resources.
dev.stop
```

### Style info

I use a somewhat non-standard style for my code.

- Keep lines 118 characters or shorter.  Obviously sometimes you can't, but
  please try your best.  Use 80 or 115 characters for Markdown files, though.
- Please use `pascalCase` for variable and method names, `CamelCase` for type
  names, and `UPPER_SNAKE_CASE` for constants.
- Put parentheses around method parameters, except for these methods: `puts`,
  `pp`, `p`, `raise`, `sleep`, `spawn`, `loop`, and `exit`.
- Always the full `do |foo|...end` syntax with blocks, except when it's all on
  one line or an anonymous function, then use either { and } or the normal `do
  |foo|...end`.
- The type name for exceptions end with `Error`.  For example,
  `ExternalProgramError`.
- Add type information everywhere you can.
- Uppercase hex literals except where 8 might be mistaken for a capital 'b'.
- No tabs, always spaces.

### How do I contribute?

1. Go to [https://nanako.mooo.com/fossil/remisound]() and clone the Fossil
   repository.
2. Create a new branch for your feature.
3. Push locally to the new branch.
4. Create a [bundle](https://fossil-scm.org/home/help?cmd=bundle) with Fossil
   that contains your changes.
5. Get in contact with me.

## Contributors

* Remilia Scarlet - creator and maintainer
  * Homepage: [https://remilia.sdf.org/](https://remilia.sdf.org/)
  * Mastodon: [@MistressRemilia@social.sdf.org](https://social.sdf.org/@MistressRemilia)
  * Email: zremiliaz@postzeoz.jpz  My real address does not contain Z's

